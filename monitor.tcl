#### Notify list ####
#
# 

namespace eval nlist {

    # Set here the nick you use on IRC
    variable owner "JohnDoe"
    

    proc deladd {nick type victs} {
        putlog "doing $type for [join $victs ", "]"
        foreach vict $victs {
            monitor $type $vict
            if {$type eq "add"} {
                bind MONITOR - $vict ::nlist::notify
            } else {
                unbind MONITOR - $vict ::nlist::notify
            }
        }
        ::nlist::nlist $nick
    }
    
    proc notify {nick online} {
        if {$online == 1} {
            set status "online"
        } else {
            set status "offline"
        }
        putserv "PRIVMSG $::nlist::owner :$nick is now $status"
    }
    
    # manual run of status
    # faking a monitor message
    proc status {nick vict} {
        ::nlist::notify $vict [monitor status $vict]
    }
    
    proc clear {nick} {
        putlog "cleaning notify list"
        monitor clear
        putserv "PRIVMSG $nick :Notify list cleared"
    }
    
    proc nlist {nick} {
        putlog "List of monitors for $nick"
        putserv "PRIVMSG $nick :[monitor list]"
    }
    
    proc nlcmd {nick uhost handle text} {
        set args [split $text]
        putlog $args
        if {[llength $args] == 0} {
            set args {default}
        }
        set cmd [string tolower [lindex $args 0]]
        switch $cmd {
            "add" -
            "delete" {
                if {[llength $args]<2} {
                    puthelp "PRIVMSG $nick :Command is \002!notify [string tolower $cmd] <nick>\002"
                    puthelp "PRIVMSG $nick :You can put several nicks separated with a space"
                    return
                }
                ::nlist::deladd $nick $cmd [lrange $args 1 end]
            }
            "clear" {
                ::nlist::clear $nick
            }
            "list" {
                ::nlist::nlist $nick
            }
            "status" {
                if {[llength $args]<2} {
                    puthelp "PRIVMSG $nick :Command is \002!notify status <nick>\002"
                    return
                }
                ::nlist::status $nick [lrange $args 1 end]
            }
            default {
                putserv "PRIVMSG $nick :/msg $::botnick !notify <add|delete|clear|list|status>"
            }
        }
    }
    
    bind msg n "!notify" ::nlist::nlcmd

}
