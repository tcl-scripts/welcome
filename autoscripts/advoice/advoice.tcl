#
# Auto Devoice 1.4
#
# Author: CrazyCat <crazycat@c-p-f.org>
# https://www.eggdrop.fr
# ircs://irc.zeolia.chat/eggdrop
#
# Auto voice users on join
# Auto devoice users after an idle delay
#
# Usage:
#	- Console:
#	 .chanset #channel advoice XX
# With XX in minutes (set to 0 to disable on the channel)
#	 .chanset #channel +/-joinvoice
# to enable/disable voice on join
# - Message:
#	 You need to be known as channel operator in eggdrop
#	 or to have the operator status on the channel
#	 Help:
#		 /msg eggdrop delay help
#	 Enable:
#		 /msg eggdrop delay #channel on (use default value)
#		 /msg eggdrop delay #channel XX
#	 Disable:
#		 /msg eggdrop delay #channel off|0
#	 Turn on/off voice on join:
#		 /msg eggdrop voj #channel on|off
#
# N.B.: the advoice default setting is 0 (disabled)

# Changelog
# 1.4 : make it working with autoscripts
# 1.3 : added /msg management of voiceonjoin
# 1.2 : added flag voiceonjoin
# 1.1 : corrected some returns which blocked the log

# TODO
# Implement the ChanServ (de)voice 

namespace eval advoice {

   # Default idle time
   # variable defdelay 30

   # May we deop inactive ops ?
   # variable opasvoice 0

   # (Not implemented) Use chanserv to deop ? If yes, fill with ChanServ nickname
   # This will force the progressive mode to be enabled (to avoid flood)
   # variable chanserv ""
   
   # Progressive mode enabled: the script removes modes one per one
   # disabled: all modes are removed in one time
   #variable progressive 0

   # Protected users
   variable protected "\[Gamer\] \[Guru\] $::botnick"

   ##############################
   #									  #
   # DO NOT EDIT ANYTHING BELOW #
   #									  #
   ##############################

   setudef int advdelay
   setudef flag advjoin
   
   variable author "CrazyCat"
   variable versionNum "1.4"
   variable versionName "AutoDeVoice"

   bind join - * ::advoice::avjoin
   bind time - * ::advoice::check
   bind pubm - * ::advoice::pub
   # bind ctcp - "ACTION" ::advoice::act
   # now useless as pubm catches ACTION
   bind msg - "delay" ::advoice::setdelay
   bind msg - "voj" ::advoice::setvoj

   proc setdelay {nick uhost handle text} {
      set args [split $text]
      set chan [string tolower [lindex $args 0]]
      if {![string first $chan "#"] == -1 && $chan ne "help"} {
         putserv "PRIVMSG $nick :Please, type /msg $::botnick delay help"
         return 0
      }
      if { $chan eq "help"} {
         putserv "PRIVMSG $nick :\002/msg $::botnick delay #chan on\002 to turn the AutoDeVoice on (with default delay: $::advoice::default minutes)";
         putserv "PRIVMSG $nick :\002/msg $::botnick delay #chan off|0\002 to turn the AutoDeVoice off";
         putserv "PRIVMSG $nick :\002/msg $::botnick delay #chan XX\002 to turn the AutoDeVoice on with a delay of XX minutes (between 1 and 999)";
         putserv "PRIVMSG $nick :\002/msg $::botnick delay #chan\002 to see the status"
         return 0
      }
      if {![::advoice::isoperator $nick $chan]} { return }
      set delay [lindex $args 1]
      switch -regexp -- $delay {
         ^on$ {
            channel set $chan advdelay $::advoice::defdelay
            putserv "PRIVMSG $nick :Delay is now [channel get $chan advoice] minutes"
         }
         ^off$ -
         ^0$ {
            channel set $chan advdelay 0
            putserv "PRIVMSG $nick :Ok, $::advoice::versionName disabled"
         }
         {^\d{1,3}$} {
            channel set $chan advdelay $delay
            putserv "PRIVMSG $nick :Delay is now [channel get $chan advoice] minutes"
         }
         default {putserv "PRIVMSG $nick :The idle delay on $chan is actually [channel get $chan advdelay] minutes";}
      }
   }

   proc setvoj {nick uhost handle text} {
      set args [split $text]
      set chan [string tolower [lindex $args 0]]
      if {![string first $chan "#"] == -1 || $chan eq ""} {
         putserv "PRIVMSG $nick :Please, type /msg $::botnick voj #channel on|off"
         return 0
      }
      if {![::advoice::isoperator $nick $chan]} { return }
      set flag [lindex $args 1]
      switch -nocase -- $flag {
         on { channel set $chan +joinvoice }
         off { channel set $chan -joinvoice }
         default {
            putserv "PRIVMSG $nick :Please, type /msg $::botnick voj #channel on|off"
            return
         }
      }
      putserv "PRIVMSG $nick :Voice on join is now turned $flag in $chan"
   }

   proc isoperator {nick chan} {
      if {![validchan $chan]} {
         putserv "PRIVMSG $nick :Sorry, I'm not on $chan"
         return 0
      }
      if {![isop $nick $chan] && ![wasop $nick $chan] && ![matchattr $handle o $chan]} {
         putserv "PRIVMSG $nick :Sorry, you're not operator (@) on $chan"
         return 0
      }
      return 1
   }

   proc avjoin {nick uhost handle chan} {
      if {![channel get $chan advjoin]} {
         return
      }
      if {[lsearch -nocase [::advoice::filt [split $::advoice::protected]] [::advoice::filt [split $nick]]] >= 0} {
         return
      }
      if {[channel get $chan advdelay]==0} {
         return
      }
      after 5000 {set delayed ok}
      vwait delayed
      if {![isop $nick $chan] && ![ishalfop $nick $chan]} {
         ::advoice::voice $chan $nick
      }
   }

   proc check {min hour day month year} {
      foreach chan [channels] {
         set idlemax [channel get $chan advdelay]
         if {$idlemax==0} { continue; }
         foreach nick [chanlist $chan] {
            if {[lsearch -nocase [split [::advoice::filt $::advoice::protected]] [::advoice::filt [split $nick]]] >= 0} { continue }
            if {[isop $nick $chan]} {
               if {$::advoice::opasvoice == 0} {
                  continue
               } elseif {[getchanidle $nick $chan] > $idlemax} {
                  pushmode $chan -o $nick
                  ::advoice::csdeop $nick $chan
                  if {$::advoice::progressive == 1 || $::advoice::chanserv ne ""} { continue }
               }
            }
            if {[ishalfop $nick $chan]} {
               if {$::advoice::opasvoice == 0} {
                  continue
               } elseif {[getchanidle $nick $chan] > $idlemax} {
                  pushmode $chan -h $nick
                  ::advoice::csdehalfop $nick $chan
                  if {$::advoice::progressive == 1 || $::advoice::chanserv ne ""} { continue }
               }
            }
            if {![isvoice $nick $chan]} { continue }
            if {[getchanidle $nick $chan] > $idlemax} {
               pushmode $chan -v $nick
               ::advoice::csdevoice $nick $chan
               if {$::advoice::progressive == 1 || $::advoice::chanserv ne ""} { continue }
            }
         }
         flushmode $chan
      }
   }

   proc act {nick uhost handle chan key text} {
      ::advoice::voice $nick $chan
   }

   proc pub {nick uhost handle chan text} {
      ::advoice::voice $nick $chan
   }
   
   proc voice {nick chan} {
      if {[channel get $chan advdelay]==0} {
         return 0
      }
      if {[lsearch -nocase [split [::advoice::filt $::advoice::protected]] [::advoice::filt [split $nick]]] >= 0} {
         return 0
      }
      if {[isop $nick $chan]} {return 0;}
      if {[ishalfop $nick $chan]} {return 0;}
      if {![isvoice $nick $chan] && ![::advoice::csvoice $nick $chan]} {
         pushmode $chan +v $nick
      }
   }

   proc csdeop {nick chan} {
      if {$::advoice::chanserv ne ""} {
         putserv "PRIVMSG $::advoice::chanserv :deop $chan $nick"
         return 1
      }
      return 0
   }

   proc csdehalfop {nick chan} {
      if {$::advoice::chanserv ne ""} {
         putserv "PRIVMSG $::advoice::chanserv :dehalfop $chan $nick"
         return 1
      }
      return 0
   }

   proc csvoice {nick chan} {
      if {$::advoice::chanserv ne ""} {
         putserv "PRIVMSG $::advoice::chanserv :voice $chan $nick"
         return 1
      }
      return 0
   }

   proc csdevoice {nick chan} {
      if {$::advoice::chanserv ne ""} {
         putserv "PRIVMSG $::advoice::chanserv :devoice $chan $nick"
         return 1
      }
      return 0
   }

   proc filt {data} {
      return [regsub -all {\W} $data {\\&}]
   }

   putlog "$::advoice::versionName $::advoice::versionNum $::advoice::author loaded"
}
