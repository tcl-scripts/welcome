### Welcome message ###
#
# Salue les personnes entrant sur les canaux
# Les messages diffèrent selon que ce soit une première arrivée,
# un retour rapide ou long

## ACTIVATION ##
# .chanset #canal +welcome

## CONFIGURATION ##
# 
# Délais
# Il y a deux délais réglables (en minutes).
# Ces délais sont comptés entre la sortie de l'utilisateur et son retour.
#   - time(few) est le délai à partir duquel un utilisateur qui revient
#       sera à nouveau salué
#   - time(long) est le délai à partir duquel on considère qu'il y a
#       longtemps que l'utilisateur est parti.
# Memento: 60min = 1 heure, 1440min = 1 jour, 10080min = 1 semaine
#
# Messages
# Les messages sont de simples textes qui peuvent prendre trois variables:
#   - %n sera remplacé par le pseudo de l'utilisateur
#   - %c sera remplacé par le canal
#   - %d sera remplacé par la durée d'absence
#
## CHANGELOG ##
# 2.1 :
#   - ajout de l'option "generic nick"
#   - ajout de la gestion du kick
# 2.0 : 
#   - retrait de la gestion du away
#   - personnalisation des messages par canal
# 1.4 : ajout de la gestion du away
# 1.3 : ajout de la durée d'absence
# 1.2 : meilleure gestion des nicks / hosts

namespace eval wlc {
   
   package require inifile
   
   variable time
   variable msg
   
   # Temps (en minutes) à partir duquel un retour
   # provoque l'affichage du message "few"
   set time(few) 60
   
   # Temps (en minutes) à partir duquel un retour
   # provoque l'affichage du message "long"
   set time(long) 1440
   
   # Message affiché lorsque l'utilisateur n'a jamais été vu
   # sur le canal
   set msg(first) "Bonjour %n, je suis le robot d'accueil. Les autres utilisateurs ne sont pas loin, tu peux déjà dire bonjour ;)"
   
   # Message affiché lorsque l'utilisateur revient
   # entre "few" minutes et "long" minutes
   set msg(few) "Hello %n, merci de revenir sur %c, tu as été absent %d"
   
   # Message affiché lorsque l'utilisateur revient
   # après plus de "long" minutes
   set msg(long) "Bonjour %n, ça fait plaisir de te revoir après %d ;)"
   
   # Message utilisé pour les pseudos génériques
   set msg(generic) "Au fait %n, tu peux prendre un pseudo plus personnel en tapant /nick <nouveau_pseudo>"

   # Masque de pseudo générique
   # accepte des wildcards comme * ou ?
   variable genick "zeol*n"
   
   # Liste des pseudos à exclure des salutations
   # Peut être sous la forme d'expression régulière
   variable known {_BS* BdS CrazyCat}
   
   # Précision: nombre d'items affichés
   set msg(dprec) 2
   
   # Séparateur des différents éléments de durée
   # celui ci est utilisé en temps normal
   set msg(dsep) ", "
   
   # Et celui ci est le dernier
   set msg(dlsep) " et "
   
   # Fonctionnement des messages:
   # - all = tous les messages (par défaut)
   # - random = un message au hasard
   set msg(mode) "random"
   
   # Emplacement des fichiers de messages
   set msg(file) "databases/"
   
   ## FIN DE CONFIGURATION ##
   
   setudef flag welcome
   setudef str wmode
   
   variable seen
   
   variable author "CrazyCat <https://www.eggdrop.fr>"
   variable version "2.1"

   proc wjoin {nick uhost handle chan} {
      set mchan [string tolower $chan]
      if {[isbotnick $nick]} { 
         ::wlc::iniload $mchan
         return
      }
      if {![channel get $mchan welcome] || [channel get $mchan welcome] eq ""} { return }
      if {[lsearch -regexp $::wlc::known $nick] != -1} { return }
      set isback 0
      set uvar "$mchan,[join [lindex [split $uhost @] 0]]"
      set nvar "$mchan,[maskhost [string tolower $nick]!$uhost 9]"
        putlog "$uvar - $nvar"
      if {[info exists ::wlc::seen($uvar)] || [info exists ::wlc::seen($nvar)]} {
         if {[info exists ::wlc::seen($uvar)]} {
            set vict $uvar
         } else {
            set vict $nvar
         }
         if {([expr [clock seconds] - $::wlc::seen($vict)])< ([expr 60 * $::wlc::time(few)])} {
            # Shut up, less than few minutes
            set isback 1
         } elseif {([expr [clock seconds] - $::wlc::seen($vict)]) < ([expr 60 * $::wlc::time(long)])} {
            # Less than a day, more than few minutes
            ::wlc::makemsg few $nick $mchan [::wlc::makeduration $::wlc::seen($vict)]
            set isback 1
         } else {
            # More than a day
            ::wlc::makemsg long $nick $mchan [::wlc::makeduration $::wlc::seen($vict)]
            if {[string match $::wlc::genick $nick]} {
               ::wlc::makemsg generic $nick $mchan ""
            }
            set isback 0
         }
      } else {
         ::wlc::makemsg first $nick $mchan ""
      }
      set ::wlc::seen($uvar) [clock seconds]
      set ::wlc::seen($nvar) [clock seconds]
   }
   
   proc wpart {nick uhost handle chan {msg ""}} {
      if {[isbotnick $nick]} { return }
      if {![channel get $chan welcome] || [channel get $chan welcome] eq ""} { return }
      if {[lsearch -regexp $::wlc::known $nick] != -1} { return }
      set mchan [string tolower $chan]
      set uvar "$mchan,[join [lindex [split $uhost @] 0]]"
      set nvar "$mchan,[maskhost [string tolower $nick]!$uhost 9]"
      set ::wlc::seen($uvar) [clock seconds]
      set ::wlc::seen($nvar) [clock seconds]
   }
   
   proc wkick {nick uhost handle chan target reason} {
      ::wlc::wpart $target [getchanhost $target $chan] * $chan $reason
   }
   
   proc makeduration {start {end 0} {prec 2}} {
      if {$end eq 0} {
         set end [clock seconds]
      }
      set out {}
      set years [expr {[clock format $end -format %Y] - [clock format $start -format %Y]}]
      set delay [clock format [expr {$end - $start}] -format "%m-%d %H:%M:%S"]
      regexp {(\d+)-(\d+) 0?(\d+):0?(\d+):0?(\d+)} $delay -> months days hours minutes seconds
      set tdata [list [incr years -1] [incr months -1] [incr days -1] [incr hours -1] $minutes $seconds]
      set i 0
      foreach val $tdata {
         if {$val > 0} {
            lappend out "$val [::wlc::dpname $val $i]"
         }
         incr i
      }
      if {$::wlc::msg(dprec) <= 2} {
         set tmpret [join [lrange $out 0 [expr {$::wlc::msg(dprec) - 1}]] $::wlc::msg(dlsep)]
      } else {
         set tmpret [join [lrange $out 0 [expr {$::wlc::msg(dprec) - 2}]] $::wlc::msg(dsep)]
         set tmpret $tmpret$::wlc::msg(dlsep)[join [lindex $out [expr {$::wlc::msg(dprec) - 1}]]]
      }
      return $tmpret
   }
   
   proc dpname {val id} {
      set lang {année mois jour heure minute seconde}
      set langs {années mois jours heures minutes secondes}
      if {$val > 1} {
         return [lindex $langs $id]
      } else {
         return [lindex $lang $id]
      }
   }
   
   proc makemsg {mode nick mchan delay} {
      if {[dict exists $::wlc::msgs($mchan) $mode]} {
         set msgs [dict get $::wlc::msgs($mchan) $mode]
         set k [dict keys $msgs]
         if {[channel get $mchan wmode] eq "random"} {
            set k [lindex $k [rand [llength $k]]]
         }
         foreach i $k {
            putserv "PRIVMSG $mchan :[format [string map {%n %1\$s %c %2\$s %d %3\$s} [dict get $msgs $i]] $nick $mchan $delay]"
         }
      }
   }
   
   proc inigen {mchan cfile} {
      set ::wlc::msgs($mchan) [dict create]
      dict set ::wlc::msgs($mchan) first 1 $::wlc::msg(first)
      dict set ::wlc::msgs($mchan) few 1 $::wlc::msg(few)
      dict set ::wlc::msgs($mchan) long 1 $::wlc::msg(long)
      dict set ::wlc::msgs($mchan) generic 1 $::wlc::msg(generic)
      ::wlc::inisave $mchan $cfile
      return
   }
   
   proc inisave {mchan cfile} {
      set fo [::ini::open $cfile -encoding utf-8 w+]
      ::ini::comment $fo "first" "" "Première venue sur le salon"
      ::ini::comment $fo "few" "" "Parti depuis peu de temps"
      ::ini::comment $fo "long" "" "Retour d'une longue absence"
      ::ini::comment $fo "generic" "" "Utilisation d'un pseudo générique"
      foreach section [dict keys [dict get $::wlc::msgs($mchan)]] {
         set i 1
         foreach k [dict keys [dict get $::wlc::msgs($mchan) $section]] {
            ::ini::set $fo $section $i [dict get $::wlc::msgs($mchan) $section $k]
            incr i
         }
      }
      ::ini::commit $fo
      ::ini::close $fo
      ::wlc::iniload $mchan
   }
   
   proc iniload {mchan} {
      set cfile "$::wlc::msg(file)[string range $mchan 1 end].ini"
      if {![file exists $cfile]} {
         ::wlc::inigen $mchan $cfile
      }
      set fi [::ini::open $cfile -encoding utf-8]
      foreach section [::ini::sections $fi] {
         dict set ::wlc::msgs($mchan) $section [::ini::get $fi $section]
      }
   }
   
   bind msg - "!wlc" ::wlc::manage
   proc manage {nick uhost handle text} {
      set args [split $text]
      if {[llength $args] <2} {
         putserv "PRIVMSG $nick :!wlc <add|del|list|move> #channel \[first|few|long\] \[id|msg\]"
         return
      }
      set cmd [join [lindex $args 0]]
      set mchan [string tolower [join [lindex $args 1]]]
      set types {first few long}
      if {![matchattr $handle +o $mchan]} {
         putlog "$handle.+ want to configure wlc for $mchan"
         return
      }
      set cfile "$::wlc::msg(file)[string range $mchan 1 end].ini"
      switch -nocase -- $cmd {
         "list" {
            if {[lsearch -nocase $types [join [lindex $args 2]]]!=-1} {
               set types [join [lindex $args 2]]
            }
            foreach t $types {
               foreach k [dict keys [dict get $::wlc::msgs($mchan) $t]] {
                  putserv "PRIVMSG $nick :\[$t\] \[$k\] [dict get $::wlc::msgs($mchan) $t $k]"
               }
            }
         }
         "add" {
            if {[lsearch -nocase $types [join [lindex $args 2]]]==-1} {
                putserv "PRIVMSG $nick :!wlc add #channel <first|few|long> msg"
                return
            } else {
                set t [string tolower [join [lindex $args 2]]]
                set l [llength [dict keys [dict get $::wlc::msgs($mchan) $t]]]
                incr l
                dict set ::wlc::msgs($mchan) $t $l [join [lrange $args 3 end]]
                ::wlc::inisave $mchan $cfile
                putserv "PRIVMSG $nick :Entry $l for $t is: [join [lrange $args 3 end]]"
            }
         }
         "del" {
            if {[lsearch -nocase $types [join [lindex $args 2]]]==-1} {
                putserv "PRIVMSG $nick :!wlc del #channel <first|few|long> msgid"
                return
            } else {
               set t [string tolower [join [lindex $args 2]]]
               if {![string is integer [join [lindex $args 3]]] || ![dict exists $::wlc::msgs($mchan) $t [join [lindex $args 3]]]} {
                  putserv "PRIVMSG $nick :[lindex $args 3] is not a valid id"
               } else {
                  set msg [dict get $::wlc::msgs($mchan) $t  [join [lindex $args 3]]]
                  dict unset ::wlc::msgs($mchan) $t [join [lindex $args 3]]
                  ::wlc::inisave $mchan $cfile
                  putserv "PRIVMSG $nick :Deleted $msg"
               }
            }
         }
         default {
            putlog "Got $text... donnot know what to do"
         }
      }
      
   }

   bind join - * ::wlc::wjoin
   bind part - * ::wlc::wpart
   bind sign - * ::wlc::wpart
   bind kick - * ::wlc::wkick
   
   putlog "Welcome.tcl v${::wlc::version} by ${::wlc::author} loaded"
}
